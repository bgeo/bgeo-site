export const environment = {
  production: true,
  tilesEndpoint: "https://tiles.brianbrown.dev",
  budget: "https://budgetapi.brianbrown.dev"
};
